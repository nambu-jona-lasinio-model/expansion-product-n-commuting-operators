(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27811,        731]
NotebookOptionsPosition[     26006,        671]
NotebookOutlinePosition[     26355,        686]
CellTagsIndexPosition[     26312,        683]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[StyleBox["Product between operators in the Mean Field \
approximation", "Chapter"]], "Text",
 CellChangeTimes->{{3.7842442891166496`*^9, 3.78424432257701*^9}, 
   3.784245694319457*^9}],

Cell["\<\
Define the operators as their Mean Field value plus a small perturbation (\
\[Epsilon] is only used as an auxiliary variable to count the powers in the \
perturbations):\
\>", "Text",
 CellChangeTimes->{{3.784241381853971*^9, 3.784241389441639*^9}, 
   3.784241432819592*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[CapitalOHat]", "[", 
   RowBox[{"O_", ",", "\[Delta]O_", ",", "\[Epsilon]_"}], "]"}], ":=", 
  RowBox[{"O", "+", 
   RowBox[{"\[Epsilon]", "*", "\[Delta]O"}]}]}]], "Input",
 CellChangeTimes->{{3.7842416122114735`*^9, 3.7842416270278473`*^9}, {
  3.7842416639064293`*^9, 3.7842416778910675`*^9}, {3.784241732916023*^9, 
  3.7842417769991355`*^9}, {3.7842418313540916`*^9, 3.7842418378406844`*^9}, {
  3.784242203618309*^9, 3.784242209439043*^9}}],

Cell[TextData[StyleBox["Product between two operators in the Mean Field \
approximation", "Section"]], "Text",
 CellChangeTimes->{{3.7842442891166496`*^9, 3.78424432257701*^9}}],

Cell["The product between two operators can be written as:", "Text",
 CellChangeTimes->{{3.7842414129367633`*^9, 3.784241429787038*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Prod2Op", "[", 
   RowBox[{
   "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", 
    "\[Epsilon]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oi", ",", "\[Delta]Oi", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oj", ",", "\[Delta]Oj", ",", "\[Epsilon]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7842419180228186`*^9, 3.7842419203126717`*^9}, {
   3.784242058559395*^9, 3.7842420705210867`*^9}, {3.7842422327949877`*^9, 
   3.7842422343913684`*^9}, {3.7842422708678637`*^9, 3.7842423486572957`*^9}, 
   3.7842424046559067`*^9, {3.784242626169859*^9, 3.7842427048285255`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  RowBox[{"Prod2Op", "[", 
   RowBox[{
    SubscriptBox["O", "1"], ",", 
    SubscriptBox["\[Delta]O", "1"], ",", 
    SubscriptBox["O", "2"], ",", 
    SubscriptBox["\[Delta]O", "2"], ",", "\[Epsilon]"}], "]"}], 
  "]"}]], "Input",
 CellChangeTimes->{
  3.7842420785462427`*^9, {3.784242327321963*^9, 3.7842423508634653`*^9}},
 NumberMarks->False],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[Delta]O", "1"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"]}]}]], "Output",
 CellChangeTimes->{{3.784241986051777*^9, 3.7842420132028913`*^9}, {
   3.7842420809789605`*^9, 3.784242098774164*^9}, {3.784242245572771*^9, 
   3.784242260542862*^9}, {3.7842423404937277`*^9, 3.784242356421295*^9}, 
   3.7842424244519014`*^9, 3.7842427136688614`*^9, 3.7842427779954147`*^9, {
   3.7842432726254683`*^9, 3.7842433011188397`*^9}, 3.7842433720652676`*^9, 
   3.784243574636415*^9, 3.7842442383552046`*^9, 3.7842443715030503`*^9, 
   3.7842456309417796`*^9, 3.7842456675450106`*^9, 3.784301955453472*^9}]
}, Open  ]],

Cell["\<\
Keep only terms that are linear in the perturbation \[Epsilon], set \
\[Epsilon]=1 and write the perturbation as \[Delta]O = \[CapitalOHat]-O\
\>", "Text",
 CellChangeTimes->{{3.784240950882122*^9, 3.784240993281459*^9}, {
  3.7842412789073544`*^9, 3.7842412812422256`*^9}, {3.784242465902052*^9, 
  3.784242511925624*^9}, {3.784244379473092*^9, 3.784244402824847*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Epsilon]2powerterm", "[", 
   RowBox[{
   "n_", ",", "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_"}], 
   "]"}], ":=", 
  RowBox[{"Coefficient", "[", 
   RowBox[{
    RowBox[{"Prod2Op", "[", 
     RowBox[{
     "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
      "\[Epsilon]"}], "]"}], ",", "\[Epsilon]", ",", "n"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7842436889473667`*^9, 3.7842437329294214`*^9}, 
   3.7842438185386667`*^9, {3.784243929747736*^9, 3.784243976442345*^9}, 
   3.784245623498268*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearizeProd2Op", "[", 
   RowBox[{"Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_"}], "]"}],
   ":=", 
  RowBox[{
   RowBox[{"Prod2Op", "[", 
    RowBox[{"Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", "1"}],
     "]"}], "-", 
   RowBox[{"\[Epsilon]2powerterm", "[", 
    RowBox[{"2", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj"}],
     "]"}]}]}]], "Input",
 CellChangeTimes->{{3.784241927054874*^9, 3.7842419743849297`*^9}, {
   3.784242090551021*^9, 3.7842420933611774`*^9}, {3.784242332918334*^9, 
   3.784242353250557*^9}, {3.7842423858921165`*^9, 3.784242390740547*^9}, {
   3.7842425418721395`*^9, 3.7842425779040074`*^9}, {3.784242732208646*^9, 
   3.784242754311629*^9}, 3.7842429601835613`*^9, {3.7842433355505185`*^9, 
   3.7842433601353693`*^9}, {3.7842438396245565`*^9, 3.784243866681949*^9}, {
   3.7842439626925526`*^9, 3.784243980822648*^9}, 3.7842456269162874`*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearProd2Op", "[", 
   RowBox[{
   "Oi_", ",", "\[CapitalOHat]i_", ",", "Oj_", ",", "\[CapitalOHat]j_"}], 
   "]"}], ":=", 
  RowBox[{"LinearizeProd2Op", "[", 
   RowBox[{"Oi", ",", 
    RowBox[{"\[CapitalOHat]i", "-", "Oi"}], ",", "Oj", ",", 
    RowBox[{"\[CapitalOHat]j", "-", "Oj"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.784244018951526*^9, 3.784244043988224*^9}, {
  3.7842440767382483`*^9, 3.7842440988432207`*^9}, {3.7842441588733606`*^9, 
  3.7842442131430655`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"LinearProd2Op", "[", 
  RowBox[{
   SubscriptBox["O", "1"], ",", 
   SubscriptBox["\[CapitalOHat]", "1"], ",", 
   SubscriptBox["O", "2"], ",", 
   SubscriptBox["\[CapitalOHat]", "2"]}], "]"}]], "Input",
 CellChangeTimes->{{3.784244256623214*^9, 3.7842442619778566`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", 
    SubscriptBox["O", "1"]}], " ", 
   SubscriptBox["O", "2"]}], "+", 
  RowBox[{
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[CapitalOHat]", "1"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["\[CapitalOHat]", "2"]}]}]], "Output",
 CellChangeTimes->{3.784244262975217*^9, 3.784244371559926*^9, 
  3.784245631011593*^9, 3.7842456676118317`*^9, 3.784301955527239*^9}]
}, Open  ]],

Cell[TextData[StyleBox["Product between three operators in the Mean Field \
approximation", "Section"]], "Text",
 CellChangeTimes->{{3.7842442891166496`*^9, 3.78424432257701*^9}, {
  3.784245103947405*^9, 3.7842451048906384`*^9}}],

Cell["The product between three operators can be written as:", "Text",
 CellChangeTimes->{{3.7842414129367633`*^9, 3.784241429787038*^9}, {
  3.784245112520622*^9, 3.784245113834262*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Prod3Op", "[", 
   RowBox[{
   "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", "Ok_", ",",
     "\[Delta]Ok_", ",", "\[Epsilon]_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oi", ",", "\[Delta]Oi", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oj", ",", "\[Delta]Oj", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Ok", ",", "\[Delta]Ok", ",", "\[Epsilon]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7842419180228186`*^9, 3.7842419203126717`*^9}, {
   3.784242058559395*^9, 3.7842420705210867`*^9}, {3.7842422327949877`*^9, 
   3.7842422343913684`*^9}, {3.7842422708678637`*^9, 3.7842423486572957`*^9}, 
   3.7842424046559067`*^9, {3.784242626169859*^9, 3.7842427048285255`*^9}, {
   3.7842451170318146`*^9, 3.784245135722007*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  RowBox[{"Prod3Op", "[", 
   RowBox[{
    SubscriptBox["O", "1"], ",", 
    SubscriptBox["\[Delta]O", "1"], ",", 
    SubscriptBox["O", "2"], ",", 
    SubscriptBox["\[Delta]O", "2"], ",", 
    SubscriptBox["O", "3"], ",", 
    SubscriptBox["\[Delta]O", "3"], ",", "\[Epsilon]"}], "]"}], 
  "]"}]], "Input",
 CellChangeTimes->{
  3.7842420785462427`*^9, {3.784242327321963*^9, 3.7842423508634653`*^9}, {
   3.7842451442097607`*^9, 3.7842451700864224`*^9}},
 NumberMarks->False],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "1"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "2"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "3"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"]}]}]], "Output",
 CellChangeTimes->{{3.7842451712995043`*^9, 3.7842451793907633`*^9}, 
   3.784245511007799*^9, 3.7842455608988047`*^9, 3.784245631067444*^9, 
   3.7842456676507277`*^9, 3.7843019555681562`*^9}]
}, Open  ]],

Cell["\<\
Keep only terms that are linear in the perturbation \[Epsilon], set \
\[Epsilon]=1 and write the perturbation as \[Delta]O = \[CapitalOHat]-O\
\>", "Text",
 CellChangeTimes->{{3.784240950882122*^9, 3.784240993281459*^9}, {
  3.7842412789073544`*^9, 3.7842412812422256`*^9}, {3.784242465902052*^9, 
  3.784242511925624*^9}, {3.784244379473092*^9, 3.784244402824847*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Epsilon]3powerterm", "[", 
   RowBox[{
   "n_", ",", "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", 
    "Ok_", ",", "\[Delta]Ok_"}], "]"}], ":=", 
  RowBox[{"Coefficient", "[", 
   RowBox[{
    RowBox[{"Prod3Op", "[", 
     RowBox[{
     "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", "Ok", ",", 
      "\[Delta]Ok", ",", "\[Epsilon]"}], "]"}], ",", "\[Epsilon]", ",", "n"}],
    "]"}]}]], "Input",
 CellChangeTimes->{{3.7842436889473667`*^9, 3.7842437329294214`*^9}, 
   3.7842438185386667`*^9, {3.784243929747736*^9, 3.784243976442345*^9}, 
   3.784245218544087*^9, {3.7842453103282795`*^9, 3.7842453462618704`*^9}, {
   3.7842454082230163`*^9, 3.7842454089381094`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearizeProd3Op", "[", 
   RowBox[{
   "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", "Ok_", ",",
     "\[Delta]Ok_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Prod3Op", "[", 
    RowBox[{
    "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", "Ok", ",", 
     "\[Delta]Ok", ",", "1"}], "]"}], "-", 
   RowBox[{"\[Epsilon]3powerterm", "[", 
    RowBox[{
    "2", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
     "Ok", ",", "\[Delta]Ok"}], "]"}], "-", 
   RowBox[{"\[Epsilon]3powerterm", "[", 
    RowBox[{
    "3", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
     "Ok", ",", "\[Delta]Ok"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.784241927054874*^9, 3.7842419743849297`*^9}, {
   3.784242090551021*^9, 3.7842420933611774`*^9}, {3.784242332918334*^9, 
   3.784242353250557*^9}, {3.7842423858921165`*^9, 3.784242390740547*^9}, {
   3.7842425418721395`*^9, 3.7842425779040074`*^9}, {3.784242732208646*^9, 
   3.784242754311629*^9}, 3.7842429601835613`*^9, {3.7842433355505185`*^9, 
   3.7842433601353693`*^9}, {3.7842438396245565`*^9, 3.784243866681949*^9}, {
   3.7842439626925526`*^9, 3.784243980822648*^9}, {3.78424535439828*^9, 
   3.784245386851614*^9}, {3.7842454278205485`*^9, 3.784245443345504*^9}, {
   3.784245545139499*^9, 3.784245548348151*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearProd3Op", "[", 
   RowBox[{
   "Oi_", ",", "\[CapitalOHat]i_", ",", "Oj_", ",", "\[CapitalOHat]j_", ",", 
    "Ok_", ",", "\[CapitalOHat]k_"}], "]"}], ":=", 
  RowBox[{"LinearizeProd3Op", "[", 
   RowBox[{"Oi", ",", 
    RowBox[{"\[CapitalOHat]i", "-", "Oi"}], ",", "Oj", ",", 
    RowBox[{"\[CapitalOHat]j", "-", "Oj"}], ",", "Ok", ",", 
    RowBox[{"\[CapitalOHat]k", "-", "Ok"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.784244018951526*^9, 3.784244043988224*^9}, {
  3.7842440767382483`*^9, 3.7842440988432207`*^9}, {3.7842441588733606`*^9, 
  3.7842442131430655`*^9}, {3.7842454572894835`*^9, 3.784245504864143*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"LinearProd3Op", "[", 
  RowBox[{
   SubscriptBox["O", "1"], ",", 
   SubscriptBox["\[CapitalOHat]", "1"], ",", 
   SubscriptBox["O", "2"], ",", 
   SubscriptBox["\[CapitalOHat]", "2"], ",", 
   SubscriptBox["O", "3"], ",", 
   SubscriptBox["\[CapitalOHat]", "3"]}], "]"}]], "Input",
 CellChangeTimes->{{3.784244256623214*^9, 3.7842442619778566`*^9}, {
  3.784245486625103*^9, 3.7842455072306447`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "2"}], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"]}], "+", 
  RowBox[{
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[CapitalOHat]", "1"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[CapitalOHat]", "2"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[CapitalOHat]", "3"]}]}]], "Output",
 CellChangeTimes->{3.784244262975217*^9, 3.784244371559926*^9, 
  3.7842455110656724`*^9, 3.7842455609581394`*^9, 3.784245631135262*^9, 
  3.7842456677195435`*^9, 3.784301955651905*^9}]
}, Open  ]],

Cell[TextData[StyleBox["Product between four operators in the Mean Field \
approximation", "Section"]], "Text",
 CellChangeTimes->{{3.7842442891166496`*^9, 3.78424432257701*^9}, {
  3.784245103947405*^9, 3.7842451048906384`*^9}, {3.784301430538355*^9, 
  3.7843014311867375`*^9}}],

Cell["The product between four operators can be written as:", "Text",
 CellChangeTimes->{{3.7842414129367633`*^9, 3.784241429787038*^9}, {
  3.784245112520622*^9, 3.784245113834262*^9}, {3.7843014386093683`*^9, 
  3.7843014392027607`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Prod4Op", "[", 
   RowBox[{
   "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", "Ok_", ",",
     "\[Delta]Ok_", ",", "Ol_", ",", "\[Delta]Ol_", ",", "\[Epsilon]_"}], 
   "]"}], ":=", 
  RowBox[{
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oi", ",", "\[Delta]Oi", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Oj", ",", "\[Delta]Oj", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Ok", ",", "\[Delta]Ok", ",", "\[Epsilon]"}], "]"}], "*", 
   RowBox[{"\[CapitalOHat]", "[", 
    RowBox[{"Ol", ",", "\[Delta]Ol", ",", "\[Epsilon]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7842419180228186`*^9, 3.7842419203126717`*^9}, {
   3.784242058559395*^9, 3.7842420705210867`*^9}, {3.7842422327949877`*^9, 
   3.7842422343913684`*^9}, {3.7842422708678637`*^9, 3.7842423486572957`*^9}, 
   3.7842424046559067`*^9, {3.784242626169859*^9, 3.7842427048285255`*^9}, {
   3.7842451170318146`*^9, 3.784245135722007*^9}, {3.784301442617696*^9, 
   3.784301461931782*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  RowBox[{"Prod4Op", "[", 
   RowBox[{
    SubscriptBox["O", "1"], ",", 
    SubscriptBox["\[Delta]O", "1"], ",", 
    SubscriptBox["O", "2"], ",", 
    SubscriptBox["\[Delta]O", "2"], ",", 
    SubscriptBox["O", "3"], ",", 
    SubscriptBox["\[Delta]O", "3"], ",", 
    SubscriptBox["O", "4"], ",", 
    SubscriptBox["\[Delta]O", "4"], ",", "\[Epsilon]"}], "]"}], 
  "]"}]], "Input",
 CellChangeTimes->{
  3.7842420785462427`*^9, {3.784242327321963*^9, 3.7842423508634653`*^9}, {
   3.7842451442097607`*^9, 3.7842451700864224`*^9}, {3.7843014857873306`*^9, 
   3.7843015038250794`*^9}},
 NumberMarks->False],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "1"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "2"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"]}], "+", 
  RowBox[{"\[Epsilon]", " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "3"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "2"], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "3"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "3"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "3"], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"], " ", 
   SubscriptBox["\[Delta]O", "4"]}], "+", 
  RowBox[{
   SuperscriptBox["\[Epsilon]", "4"], " ", 
   SubscriptBox["\[Delta]O", "1"], " ", 
   SubscriptBox["\[Delta]O", "2"], " ", 
   SubscriptBox["\[Delta]O", "3"], " ", 
   SubscriptBox["\[Delta]O", "4"]}]}]], "Output",
 CellChangeTimes->{{3.7842451712995043`*^9, 3.7842451793907633`*^9}, 
   3.784245511007799*^9, 3.7842455608988047`*^9, {3.7843014957834997`*^9, 
   3.78430150445335*^9}, 3.7843018868548403`*^9, 3.7843019556977825`*^9}]
}, Open  ]],

Cell["\<\
Keep only terms that are linear in the perturbation \[Epsilon], set \
\[Epsilon]=1 and write the perturbation as \[Delta]O = \[CapitalOHat]-O\
\>", "Text",
 CellChangeTimes->{{3.784240950882122*^9, 3.784240993281459*^9}, {
  3.7842412789073544`*^9, 3.7842412812422256`*^9}, {3.784242465902052*^9, 
  3.784242511925624*^9}, {3.784244379473092*^9, 3.784244402824847*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Epsilon]4powerterm", "[", 
   RowBox[{
   "n_", ",", "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", 
    "Ok_", ",", "\[Delta]Ok_", ",", "Ol_", ",", "\[Delta]Ol_"}], "]"}], ":=", 
  RowBox[{"Coefficient", "[", 
   RowBox[{
    RowBox[{"Prod4Op", "[", 
     RowBox[{
     "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", "Ok", ",", 
      "\[Delta]Ok", ",", "Ol", ",", "\[Delta]Ol", ",", "\[Epsilon]"}], "]"}], 
    ",", "\[Epsilon]", ",", "n"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7842436889473667`*^9, 3.7842437329294214`*^9}, 
   3.7842438185386667`*^9, {3.784243929747736*^9, 3.784243976442345*^9}, 
   3.784245218544087*^9, {3.7842453103282795`*^9, 3.7842453462618704`*^9}, {
   3.7842454082230163`*^9, 3.7842454089381094`*^9}, {3.7843015231558104`*^9, 
   3.7843015812854667`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearizeProd4Op", "[", 
   RowBox[{
   "Oi_", ",", "\[Delta]Oi_", ",", "Oj_", ",", "\[Delta]Oj_", ",", "Ok_", ",",
     "\[Delta]Ok_", ",", "Ol_", ",", "\[Delta]Ol_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Prod4Op", "[", 
    RowBox[{
    "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", "Ok", ",", 
     "\[Delta]Ok", ",", "Ol", ",", "\[Delta]Ol", ",", "1"}], "]"}], "-", 
   RowBox[{"\[Epsilon]4powerterm", "[", 
    RowBox[{
    "2", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
     "Ok", ",", "\[Delta]Ok", ",", "Ol", ",", "\[Delta]Ol"}], "]"}], "-", 
   RowBox[{"\[Epsilon]4powerterm", "[", 
    RowBox[{
    "3", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
     "Ok", ",", "\[Delta]Ok", ",", "Ol", ",", "\[Delta]Ol"}], "]"}], "-", 
   RowBox[{"\[Epsilon]4powerterm", "[", 
    RowBox[{
    "4", ",", "Oi", ",", "\[Delta]Oi", ",", "Oj", ",", "\[Delta]Oj", ",", 
     "Ok", ",", "\[Delta]Ok", ",", "Ol", ",", "\[Delta]Ol"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.784241927054874*^9, 3.7842419743849297`*^9}, {
   3.784242090551021*^9, 3.7842420933611774`*^9}, {3.784242332918334*^9, 
   3.784242353250557*^9}, {3.7842423858921165`*^9, 3.784242390740547*^9}, {
   3.7842425418721395`*^9, 3.7842425779040074`*^9}, {3.784242732208646*^9, 
   3.784242754311629*^9}, 3.7842429601835613`*^9, {3.7842433355505185`*^9, 
   3.7842433601353693`*^9}, {3.7842438396245565`*^9, 3.784243866681949*^9}, {
   3.7842439626925526`*^9, 3.784243980822648*^9}, {3.78424535439828*^9, 
   3.784245386851614*^9}, {3.7842454278205485`*^9, 3.784245443345504*^9}, {
   3.784245545139499*^9, 3.784245548348151*^9}, {3.784301667183092*^9, 
   3.7843017466898885`*^9}, {3.7843018618917723`*^9, 3.7843018751972246`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LinearProd4Op", "[", 
   RowBox[{
   "Oi_", ",", "\[CapitalOHat]i_", ",", "Oj_", ",", "\[CapitalOHat]j_", ",", 
    "Ok_", ",", "\[CapitalOHat]k_", ",", "Ol_", ",", "\[CapitalOHat]l_"}], 
   "]"}], ":=", 
  RowBox[{"LinearizeProd4Op", "[", 
   RowBox[{"Oi", ",", 
    RowBox[{"\[CapitalOHat]i", "-", "Oi"}], ",", "Oj", ",", 
    RowBox[{"\[CapitalOHat]j", "-", "Oj"}], ",", "Ok", ",", 
    RowBox[{"\[CapitalOHat]k", "-", "Ok"}], ",", "Ol", ",", 
    RowBox[{"\[CapitalOHat]l", "-", "Ol"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.784244018951526*^9, 3.784244043988224*^9}, {
  3.7842440767382483`*^9, 3.7842440988432207`*^9}, {3.7842441588733606`*^9, 
  3.7842442131430655`*^9}, {3.7842454572894835`*^9, 3.784245504864143*^9}, {
  3.784301757320018*^9, 3.7843017924420023`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"LinearProd4Op", "[", 
  RowBox[{
   SubscriptBox["O", "1"], ",", 
   SubscriptBox["\[CapitalOHat]", "1"], ",", 
   SubscriptBox["O", "2"], ",", 
   SubscriptBox["\[CapitalOHat]", "2"], ",", 
   SubscriptBox["O", "3"], ",", 
   SubscriptBox["\[CapitalOHat]", "3"], ",", 
   SubscriptBox["O", "4"], ",", 
   SubscriptBox["\[CapitalOHat]", "4"]}], "]"}]], "Input",
 CellChangeTimes->{{3.784244256623214*^9, 3.7842442619778566`*^9}, {
  3.784245486625103*^9, 3.7842455072306447`*^9}, {3.784301799746115*^9, 
  3.7843018107327824`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "3"}], " ", 
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"]}], "+", 
  RowBox[{
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[CapitalOHat]", "1"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[CapitalOHat]", "2"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "4"], " ", 
   SubscriptBox["\[CapitalOHat]", "3"]}], "+", 
  RowBox[{
   SubscriptBox["O", "1"], " ", 
   SubscriptBox["O", "2"], " ", 
   SubscriptBox["O", "3"], " ", 
   SubscriptBox["\[CapitalOHat]", "4"]}]}]], "Output",
 CellChangeTimes->{3.784244262975217*^9, 3.784244371559926*^9, 
  3.7842455110656724`*^9, 3.7842455609581394`*^9, 3.784301813774987*^9, 
  3.7843018869116707`*^9, 3.7843019557795906`*^9}]
}, Open  ]]
},
WindowSize->{1402, 997},
WindowMargins->{{199, Automatic}, {Automatic, 22}},
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 199, 3, 61, "Text"],
Cell[760, 25, 285, 6, 30, "Text"],
Cell[1048, 33, 482, 9, 30, "Input"],
Cell[1533, 44, 177, 2, 53, "Text"],
Cell[1713, 48, 136, 1, 30, "Text"],
Cell[1852, 51, 698, 14, 30, "Input"],
Cell[CellGroupData[{
Cell[2575, 69, 387, 11, 30, "Input"],
Cell[2965, 82, 980, 21, 30, "Output"]
}, Open  ]],
Cell[3960, 106, 379, 6, 30, "Text"],
Cell[4342, 114, 569, 14, 30, "Input"],
Cell[4914, 130, 954, 18, 30, "Input"],
Cell[5871, 150, 517, 12, 30, "Input"],
Cell[CellGroupData[{
Cell[6413, 166, 295, 7, 30, "Input"],
Cell[6711, 175, 448, 13, 30, "Output"]
}, Open  ]],
Cell[7174, 191, 230, 3, 53, "Text"],
Cell[7407, 196, 187, 2, 30, "Text"],
Cell[7597, 200, 888, 17, 30, "Input"],
Cell[CellGroupData[{
Cell[8510, 221, 517, 14, 30, "Input"],
Cell[9030, 237, 1449, 40, 30, "Output"]
}, Open  ]],
Cell[10494, 280, 379, 6, 30, "Text"],
Cell[10876, 288, 740, 16, 30, "Input"],
Cell[11619, 306, 1361, 27, 30, "Input"],
Cell[12983, 335, 664, 13, 30, "Input"],
Cell[CellGroupData[{
Cell[13672, 352, 425, 10, 30, "Input"],
Cell[14100, 364, 732, 21, 30, "Output"]
}, Open  ]],
Cell[14847, 388, 280, 4, 53, "Text"],
Cell[15130, 394, 239, 3, 30, "Text"],
Cell[15372, 399, 1080, 21, 30, "Input"],
Cell[CellGroupData[{
Cell[16477, 424, 647, 17, 30, "Input"],
Cell[17127, 443, 3385, 95, 50, "Output"]
}, Open  ]],
Cell[20527, 541, 379, 6, 30, "Text"],
Cell[20909, 549, 857, 17, 30, "Input"],
Cell[21769, 568, 1794, 33, 69, "Input"],
Cell[23566, 603, 817, 16, 30, "Input"],
Cell[CellGroupData[{
Cell[24408, 623, 555, 13, 30, "Input"],
Cell[24966, 638, 1024, 30, 30, "Output"]
}, Open  ]]
}
]
*)

